package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/go-kit/kit/log/level"
	lru "github.com/hashicorp/golang-lru"
	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/latency.at/latencyAt/errors"
)

const (
	bookerResultChanBuffer = 1024
	cacheSize              = 10240
)

var (
	requestCachedCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "lat",
			Subsystem: "exporter",
			Name:      "requests_cached_total",
			Help:      "Total number of cached requests made by latency exporter",
		},
	)
	requestCacheDepth = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "lat",
			Subsystem: "exporter",
			Name:      "requests_cache_depth",
			Help:      "Current dept of request cache",
		},
	)
)

func init() {
	prometheus.MustRegister(requestCachedCounter)
	prometheus.MustRegister(requestCacheDepth)
}

type Booker struct {
	BookURL  string
	topic    *pubsub.Topic
	ctx      context.Context
	resultCh chan *pubsub.PublishResult
	cache    *lru.ARCCache
}

type cacheEntry struct {
	status int
	expiry time.Time
}

func newBooker(bookURL, projectID, topicName string) (*Booker, error) {
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		return nil, err
	}

	cache, err := lru.NewARC(cacheSize)
	if err != nil {
		return nil, err
	}
	b := &Booker{
		BookURL:  bookURL,
		topic:    client.Topic(topicName),
		ctx:      ctx,
		resultCh: make(chan *pubsub.PublishResult, bookerResultChanBuffer),
		cache:    cache,
	}
	go b.watchResults()
	return b, nil
}

func (b *Booker) watchResults() {
	for {
		res := <-b.resultCh
		id, err := res.Get(b.ctx)
		if err != nil {
			errorCounter.Inc()
			level.Error(logger).Log("msg", "Publishing message failed", "error", err)
			continue
		}
		level.Debug(logger).Log("msg", "Published message", "id", id)
	}
}

func (b *Booker) validateHTTP(token string) (error, int) {
	resp, err := http.Post(b.BookURL+"/"+token, "text/json", nil)
	if err != nil {
		return err, http.StatusInternalServerError
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("Request failed and couldn't read body: %s", err), http.StatusInternalServerError
		}
		return fmt.Errorf("%s", body), resp.StatusCode
	}
	return nil, http.StatusOK
}

func (b *Booker) Validate(req *http.Request) (error, int) {
	fields := strings.Fields(req.Header.Get("Authorization")) // Token <token>
	if len(fields) != 2 {
		return errors.ErrAuthorizationMissing, http.StatusBadRequest
	}
	if fields[0] != "Bearer" {
		return errors.ErrAuthorizationMissing, http.StatusBadRequest
	}
	token := fields[1]

	e, found := b.cache.Get(token)
	if found {
		ce := e.(*cacheEntry)
		status := ce.status
		if time.Now().Before(ce.expiry) {
			// Fresh
			switch status {
			case http.StatusOK:
				return nil, status

			case http.StatusPaymentRequired:
				return errors.ErrBalanceLow, status

			case http.StatusForbidden:
				return errors.ErrBalanceLow, status
			}
			// For all other errors, we re-validate.
		}
	}
	err, status := b.validateHTTP(token)
	b.cache.Add(token, &cacheEntry{status, time.Now().Add(1 * time.Minute)})
	requestCacheDepth.Set(float64(b.cache.Len()))
	//	b.cache.Set(token, status, cache.DefaultExpiration)
	return err, status
}

func (b *Booker) Log(req *http.Request) {
	b.resultCh <- b.topic.Publish(b.ctx, &pubsub.Message{
		Attributes: reqToAttribs(req),
	})
}

func (b *Booker) Stop() {
	b.topic.Stop()
}

func reqToAttribs(r *http.Request) map[string]string {
	params := r.URL.Query()
	target := params.Get("target")
	module := params.Get("module")
	return map[string]string{
		"proto":         r.Proto,
		"target":        target,
		"module":        module,
		"encoding":      r.Header.Get("Accept-Encoding"),
		"authorization": r.Header.Get("Authorization"),
	}
}
