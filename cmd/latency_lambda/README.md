# Lambda
##################### TODO ####################
- looks like stuff is working when using 'proxy integration'
- implement
  https://stackoverflow.com/questions/39772259/how-do-i-cloudform-an-api-gateway-resource-with-a-lambda-proxy-integration
- 
###############################################

## Generate config
```
//go:generate ../config_dump/config_dump -c blackbox.yml -o config.go
sed -i 's/config.\(HTTPClient\|TLS\)Config/p&/g' config.go
sed -i 's/config.\(BasicAuth\|URL\)/p&/g' config.go 
```

## Deploy Stack
Since the code needs to exist when we create the lambda, on first run we disable
it's creation by setting FirstRun=true:

### Deploy stack w/o Lambda
```
aws cloudformation create-stack \
  --capabilities CAPABILITY_IAM \
  --stack-name test-lambda \
  --parameters ParameterKey=FirstRun,ParameterValue=true \
  --template-body "$(cat cfn/site.yml)"
```

### Upload Lambda
```
make
aws s3 cp handler.zip s3://latency.at-lambda-prober/handler.zip
```

### Bump handler.zip
```
fn=$(aws lambda list-functions \
  | jq -r '.Functions[]|select(.FunctionName|test("^test-lambda-Prober-")).FunctionArn')

aws lambda update-function-code \
  --function-name "${fn}" \
  --s3-bucket "latency.at-lambda-prober" \
  --s3-key    "handler.zip"
```


### Update stack to enable Lambda
```
aws cloudformation update-stack \
  --capabilities CAPABILITY_IAM \
  --stack-name test-lambda \
  --template-body "$(cat cfn/site.yml)"
```

## Test

### Invoke
```
aws lambda invoke \
  --function-name golang-preview \
  --invocation-type RequestResponse \
  --log-type Tail  /dev/stderr \
  --query 'LogResult' \
  --output text
```

## Preview
```
wget -qO- https://github.com/eawsy/aws-lambda-go-shim/raw/master/src/preview.bash | bash
```
Delete again:
```
aws lambda delete-function  --function-name golang-preview
```
