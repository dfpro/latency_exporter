FROM golang:1.9
ARG BASIC_AUTH
RUN go get -u github.com/kardianos/govendor
WORKDIR /go/src/gitlab.com/latency.at/latency_exporter/

COPY vendor/ vendor/
RUN  govendor sync

COPY .  .
RUN cd cmd/latency_exporter && CGO_ENABLED=0 go build

FROM alpine:3.6
RUN apk add --update ca-certificates && adduser -D user
# USER user # FIXME: We need to bind priovileged ports right now
COPY --from=0 /go/src/gitlab.com/latency.at/latency_exporter/cmd/latency_exporter/latency_exporter /
ENTRYPOINT [ "/latency_exporter" ]
